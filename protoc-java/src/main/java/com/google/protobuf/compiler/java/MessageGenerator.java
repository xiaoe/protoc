package com.google.protobuf.compiler.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.google.protobuf.DescriptorProtos.DescriptorProto;
import com.google.protobuf.DescriptorProtos.FieldDescriptorProto;
import com.google.protobuf.DescriptorProtos.OneofDescriptorProto;
import com.google.protobuf.DescriptorProtos.SourceCodeInfo;
import com.google.protobuf.DescriptorProtos.SourceCodeInfo.Location;
import com.google.protobuf.compiler.java.FieldGenerator.EnumFieldGenerator;
import com.google.protobuf.compiler.java.FieldGenerator.MapFieldGenerator;
import com.google.protobuf.compiler.java.FieldGenerator.MessageFieldGenerator;
import com.google.protobuf.compiler.java.FieldGenerator.PrimitiveFieldGenerator;
import com.google.protobuf.compiler.java.FieldGenerator.RepeatedEnumFieldGenerator;
import com.google.protobuf.compiler.java.FieldGenerator.RepeatedMessageFieldGenerator;
import com.google.protobuf.compiler.java.FieldGenerator.RepeatedPrimitiveFieldGenerator;

/**
 * 
 * 
 * @author mingkai.zhou
 */
public class MessageGenerator implements IGenerator {
	private DescriptorProto _descriptor;
	private Map<String, String> _classRef;
	private String _scope;
	private HashMap<String, DescriptorProto> _nestedTypes = new HashMap<>();
	private ArrayList<FieldGenerator> _fieldGenerators = new ArrayList<>();
	private HashMap<String, String> _variables = new HashMap<>();
	private String leadingComments;// 上面注释
	private String trailingComments;// 右面注释

	public MessageGenerator(DescriptorProto descriptor, Map<String, String> classRef, String packageName, String scope, SourceCodeInfo sourceCodeInfo, int index) {
		_descriptor = descriptor;
		_classRef = classRef;

		List<Integer> path = Arrays.asList(4, index);
		for (Location l : sourceCodeInfo.getLocationList()) {
			if (l.getPathCount() == 2 && l.getPathList().equals(path)) {
				leadingComments = l.getLeadingComments();
				trailingComments = l.getTrailingComments();
			}
		}

		_variables.put("class", Util.makeASClassName(scope, descriptor.getName()));
		_variables.put("package", packageName);

		_scope = Util.makeScope(scope, descriptor.getName());
		for (DescriptorProto nestedType : _descriptor.getNestedTypeList()) {
			String qcn = Util.makeQualifiedClassName(packageName, _scope, nestedType.getName());
			_nestedTypes.put(qcn, nestedType);
		}

		// Make属性
		makeFieldGenerators(sourceCodeInfo, index);
	}

	@Override
	public void generate(Printer printer) {
		Util.getFileText("xxx").ifPresent(text -> printer.writeln(text));
		printer.writeln(_variables, "package #package#;");
		printer.writeln();

		// import class
		generateImportClass(printer);
		printer.writeln();

		printer.writeln("/**");
		if (!leadingComments.isEmpty()) {
			printer.writeln(" * " + leadingComments.trim());
			printer.writeln(" *");
		}
		printer.writeln(" * @author 自动化工具(墨鹍科技)");
		printer.writeln(" */");
		// class
		boolean isPacket = _descriptor.getName().endsWith("_CS") || _descriptor.getName().endsWith("_SC") || _descriptor.getName().endsWith("_SS");
		printer.writeln(_variables, "public class #class# " + (isPacket ? "extends AbstractPacket {" : "implements ProtobufSerializable {") + (trailingComments.isEmpty() ? "" : " // " + trailingComments.trim()));
		printer.indent();

		// oneof
		for (OneofDescriptorProto oneof : _descriptor.getOneofDeclList()) {
			HashMap<String, String> variables = new HashMap<>();
			variables.put("oneof_name", oneof.getName());
			variables.put("oneof_capitalized_name", Util.makeCapitalizedName(oneof.getName()));
			variables.put("oneof_case", oneof.getName() + "Case");

			printer.writeln(variables, "" + "private var _#oneof_case#:int = 0;\n" + "private var _#oneof_name#:* = null;\n" + "public function get #oneof_case#():int {\n" + "    return _#oneof_case#;\n" + "}\n"
					+ "public function clean#oneof_capitalized_name#():void {\n" + "    _#oneof_case# = 0;\n" + "    _#oneof_name# = null;\n" + "}");
			printer.writeln();
		}

		// getOpcode（）;
		if (isPacket) {
			printer.writeln();
			generateGetOpcode(printer, _descriptor.getName());
			printer.writeln();
		}

		// 属性区.....
		for (FieldGenerator field : _fieldGenerators) {
			field.generateAccessorCode(printer);
			printer.writeln();
		}

		// 写入输出流中...
		printer.writeln(_variables, "" + "@Override\npublic void writeTo(CodedOutputStream output) {");
		printer.indent();
		for (FieldGenerator field : _fieldGenerators) {
			field.generateWriteToCode(printer);
		}
		printer.outdent();
		printer.writeln();
		printer.writeln(_variables, "}");
		printer.writeln();

		// 从流中读出数据
		printer.writeln(_variables,
				"@Override\npublic void readFrom(CodedInputStream input) {\n" + //
						"    while(true) {\n" + "        int tag = input.readTag();\n" + //
						"        switch(tag) {\n" + //
						"            case 0: {\n" + //
						"                return;\n" + //
						"            }\n" + //
						"            default: {\n" + //
						"                if (!input.skipField(tag)) {\n" + //
						"                    return;\n" + //
						"                }\n" + //
						"                break;\n" + //
						"            }");
		printer.indent().indent().indent();
		for (FieldGenerator field : _fieldGenerators) {
			field.generateReadFromCode(printer);
		}
		printer.outdent().outdent().outdent();
		printer.writeln(_variables, "" + "        }\n" + "    }\n" + "}");
		printer.writeln();

		printer.outdent();
		printer.writeln("}"); // class
	}

	private void generateGetOpcode(Printer printer, String name) {
		boolean isServerPacket = _descriptor.getName().endsWith("_SS");
		printer.writeln("@Override");
		printer.writeln("public int getOpcode() {");
		if (isServerPacket) {
			printer.writeln("	return Opcode." + name.substring(0, name.length() - 3) + "_VALUE;");
		} else {
			printer.writeln("	return Opcode." + name.substring(0, name.length() - 3) + "_VALUE;");
		}
		printer.writeln("}");
	}

	private void generateImportClass(Printer printer) {
		HashSet<String> classSet = new HashSet<>();

		for (FieldDescriptorProto field : _descriptor.getFieldList()) {
			// 有Repeated那就是List啦...
			if (field.getLabel() == FieldDescriptorProto.Label.LABEL_REPEATED) {
				classSet.add("java.util.ArrayList");
				classSet.add("java.util.List");
			}

			switch (field.getType()) {
			case TYPE_MESSAGE:
				DescriptorProto entry = _nestedTypes.get(field.getTypeName());
				if (Util.isMapEntry(entry)) {
					for (FieldDescriptorProto keyOrValue : entry.getFieldList()) {
						classSet.add(Util.getJavaImportClass(keyOrValue, _classRef));
					}
				} else {
					classSet.add(_classRef.get(field.getTypeName()));
				}
				break;
			case TYPE_ENUM:
				classSet.add(_classRef.get(field.getTypeName()));
				break;
			case TYPE_BYTES:
				classSet.add("flash.utils.ByteArray");
				break;
			default:
				break;
			}
		}

		classSet.add("xyz.noark.protobuf.*");
		classSet.remove(null);

		String[] imports = classSet.toArray(new String[classSet.size()]);
		Arrays.sort(imports);

		for (String clazz : imports) {
			if (clazz != null) {
				printer.writeln("import %s;", clazz);
			}
		}
	}

	private void makeFieldGenerators(SourceCodeInfo sourceCodeInfo, int classIndex) {
		for (int i = 0; i < _descriptor.getFieldCount(); i++) {
			FieldDescriptorProto field = _descriptor.getFieldList().get(i);

			// 属性嘛只要右边的注解
			String trailingComments = "";
			List<Integer> path = Arrays.asList(4, classIndex, 2, i);
			for (Location l : sourceCodeInfo.getLocationList()) {
				if (l.getPathCount() == 4 && l.getPathList().equals(path)) {
					trailingComments = l.getTrailingComments();
				}
			}

			// 数组类型的属性...
			if (field.getLabel() == FieldDescriptorProto.Label.LABEL_REPEATED) {
				switch (field.getType()) {
				case TYPE_MESSAGE:
					DescriptorProto mapDescriptor = _nestedTypes.get(field.getTypeName());
					if (Util.isMapEntry(mapDescriptor)) {
						_fieldGenerators.add(new MapFieldGenerator(field, mapDescriptor, _scope, _classRef));
					} else {
						_fieldGenerators.add(new RepeatedMessageFieldGenerator(field, _classRef, trailingComments));
					}
					break;
				case TYPE_ENUM:
					_fieldGenerators.add(new RepeatedEnumFieldGenerator(field, _classRef, trailingComments));
					break;
				default:
					_fieldGenerators.add(new RepeatedPrimitiveFieldGenerator(field, trailingComments));
					break;
				}
			}
			// oneof 未实现...
			else if (field.hasOneofIndex()) {
				String oneof = _descriptor.getOneofDecl(field.getOneofIndex()).getName();
				throw new RuntimeException("oneof 未实现..." + oneof);
			}
			// 原始属性...
			else {
				switch (field.getType()) {
				case TYPE_MESSAGE:
					_fieldGenerators.add(new MessageFieldGenerator(field, _classRef));
					break;
				case TYPE_ENUM:
					_fieldGenerators.add(new EnumFieldGenerator(field, _classRef, trailingComments));
					break;
				default:
					_fieldGenerators.add(new PrimitiveFieldGenerator(field, trailingComments));
					break;
				}
			}
		}
	}
}