package com.google.protobuf.compiler.java;

public interface IGenerator {
    void generate(Printer printer);
}
