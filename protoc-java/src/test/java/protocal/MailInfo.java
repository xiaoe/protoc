/*
 * Copyright © 2016 MoKun All Rights Reserved.
 * 
 * 感谢您加入墨鹍科技，不用多久，您就会升职加薪、当上总经理、出任CEO、迎娶白富美、从此走上人生巅峰
 * 除非符合本公司的商业许可协议，否则不得使用或传播此源码，您可以下载许可协议文件：
 * 
 * 		http://www.noark.xyz/mokun/LICENSE
 *
 * 1、未经许可，任何公司及个人不得以任何方式或理由来修改、使用或传播此源码;
 * 2、禁止在本源码或其他相关源码的基础上发展任何派生版本、修改版本或第三方版本;
 * 3、无论你对源代码做出任何修改和优化，版权都归墨鹍科技所有，我们将保留所有权利;
 * 4、凡侵犯墨鹍科技相关版权或著作权等知识产权者，必依法追究其法律责任，特此郑重法律声明！
 */
package protocal;

import java.util.ArrayList;
import java.util.List;
import protocal.StoreGoodsStruct;
import xyz.noark.protobuf.*;

/**
 * 每封邮件的内容
 *
 * @author 自动化工具(墨鹍科技)
 */
public class MailInfo implements ProtobufSerializable {
    // 邮件唯一标识
    private long id = 0;
    
    /**
     * 获取邮件唯一标识
     *
     * @return 邮件唯一标识
     */
    public long getId() {
        return id;
    }
    
    /**
     * 设置邮件唯一标识
     *
     * @param id 邮件唯一标识
     */
    public void setId(long id) {
        this.id = id;
    }

    // 发件人唯一标识
    private long sendId = 0;
    
    /**
     * 获取发件人唯一标识
     *
     * @return 发件人唯一标识
     */
    public long getSendId() {
        return sendId;
    }
    
    /**
     * 设置发件人唯一标识
     *
     * @param sendId 发件人唯一标识
     */
    public void setSendId(long sendId) {
        this.sendId = sendId;
    }

    // 发件人姓名，为空则认为是系统
    private String sendName = "";
    
    /**
     * 获取发件人姓名，为空则认为是系统
     *
     * @return 发件人姓名，为空则认为是系统
     */
    public String getSendName() {
        return sendName;
    }
    
    /**
     * 设置发件人姓名，为空则认为是系统
     *
     * @param sendName 发件人姓名，为空则认为是系统
     */
    public void setSendName(String sendName) {
        this.sendName = sendName;
    }

    // 邮件标题
    private String title = "";
    
    /**
     * 获取邮件标题
     *
     * @return 邮件标题
     */
    public String getTitle() {
        return title;
    }
    
    /**
     * 设置邮件标题
     *
     * @param title 邮件标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    // 邮件文本内容
    private String content = "";
    
    /**
     * 获取邮件文本内容
     *
     * @return 邮件文本内容
     */
    public String getContent() {
        return content;
    }
    
    /**
     * 设置邮件文本内容
     *
     * @param content 邮件文本内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    // 附件列表
    private List<StoreGoodsStruct> attachment = new ArrayList<>();
    
    /**
     * 获取附件列表
     *
     * @return 附件列表
     */
    public List<StoreGoodsStruct> getAttachment() {
        return attachment;
    }
    

    // 附件是否领取状态：0未领取1已领取
    private int attachmentStatus = 0;
    
    /**
     * 获取附件是否领取状态：0未领取1已领取
     *
     * @return 附件是否领取状态：0未领取1已领取
     */
    public int getAttachmentStatus() {
        return attachmentStatus;
    }
    
    /**
     * 设置附件是否领取状态：0未领取1已领取
     *
     * @param attachmentStatus 附件是否领取状态：0未领取1已领取
     */
    public void setAttachmentStatus(int attachmentStatus) {
        this.attachmentStatus = attachmentStatus;
    }

    // 阅读状态：0未读1已读
    private int read = 0;
    
    /**
     * 获取阅读状态：0未读1已读
     *
     * @return 阅读状态：0未读1已读
     */
    public int getRead() {
        return read;
    }
    
    /**
     * 设置阅读状态：0未读1已读
     *
     * @param read 阅读状态：0未读1已读
     */
    public void setRead(int read) {
        this.read = read;
    }

    // 邮件创建时间，unix时间戳，单位：秒
    private long createTime = 0;
    
    /**
     * 获取邮件创建时间，unix时间戳，单位：秒
     *
     * @return 邮件创建时间，unix时间戳，单位：秒
     */
    public long getCreateTime() {
        return createTime;
    }
    
    /**
     * 设置邮件创建时间，unix时间戳，单位：秒
     *
     * @param createTime 邮件创建时间，unix时间戳，单位：秒
     */
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    // 邮件过期时间，unix时间戳，单位：秒
    private long deadline = 0;
    
    /**
     * 获取邮件过期时间，unix时间戳，单位：秒
     *
     * @return 邮件过期时间，unix时间戳，单位：秒
     */
    public long getDeadline() {
        return deadline;
    }
    
    /**
     * 设置邮件过期时间，unix时间戳，单位：秒
     *
     * @param deadline 邮件过期时间，unix时间戳，单位：秒
     */
    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    @Override
    public void writeTo(CodedOutputStream output) {
        if (id != 0) {
            output.writeFixed64(9, id);
        }
        if (sendId != 0) {
            output.writeFixed64(17, sendId);
        }
        if (sendName.length() > 0) {
            output.writeString(26, sendName);
        }
        if (title.length() > 0) {
            output.writeString(34, title);
        }
        if (content.length() > 0) {
            output.writeString(42, content);
        }
        if (attachment.size() > 0) {
            output.writeMessageList(50, attachment, false);
        }
        if (attachmentStatus != 0) {
            output.writeInt32(56, attachmentStatus);
        }
        if (read != 0) {
            output.writeInt32(64, read);
        }
        if (createTime != 0) {
            output.writeFixed64(73, createTime);
        }
        if (deadline != 0) {
            output.writeFixed64(81, deadline);
        }

    }

    @Override
    public void readFrom(CodedInputStream input) {
        while(true) {
            int tag = input.readTag();
            switch(tag) {
                case 0: {
                    return;
                }
                default: {
                    if (!input.skipField(tag)) {
                        return;
                    }
                    break;
                }
                case 9: {
                    this.id = input.readFixed64();
                    break;
                }
                case 17: {
                    this.sendId = input.readFixed64();
                    break;
                }
                case 26: {
                    this.sendName = input.readString();
                    break;
                }
                case 34: {
                    this.title = input.readString();
                    break;
                }
                case 42: {
                    this.content = input.readString();
                    break;
                }
                case 50: {
                    attachment.add(input.readMessage(new StoreGoodsStruct()));
                    break;
                }
                case 56: {
                    this.attachmentStatus = input.readInt32();
                    break;
                }
                case 64: {
                    this.read = input.readInt32();
                    break;
                }
                case 73: {
                    this.createTime = input.readFixed64();
                    break;
                }
                case 81: {
                    this.deadline = input.readFixed64();
                    break;
                }
            }
        }
    }

}
