/*
 * Copyright © 2016 MoKun All Rights Reserved.
 * 
 * 感谢您加入墨鹍科技，不用多久，您就会升职加薪、当上总经理、出任CEO、迎娶白富美、从此走上人生巅峰
 * 除非符合本公司的商业许可协议，否则不得使用或传播此源码，您可以下载许可协议文件：
 * 
 * 		http://www.noark.xyz/mokun/LICENSE
 *
 * 1、未经许可，任何公司及个人不得以任何方式或理由来修改、使用或传播此源码;
 * 2、禁止在本源码或其他相关源码的基础上发展任何派生版本、修改版本或第三方版本;
 * 3、无论你对源代码做出任何修改和优化，版权都归墨鹍科技所有，我们将保留所有权利;
 * 4、凡侵犯墨鹍科技相关版权或著作权等知识产权者，必依法追究其法律责任，特此郑重法律声明！
 */
package protocal;

import xyz.noark.protobuf.*;

/**
 * 仓库中的所有道具物品信息结构体
 *
 * @author 自动化工具(墨鹍科技)
 */
public class StoreGoodsStruct implements ProtobufSerializable {
    // 道具唯一ID
    private long id = 0;
    
    /**
     * 获取道具唯一ID
     *
     * @return 道具唯一ID
     */
    public long getId() {
        return id;
    }
    
    /**
     * 设置道具唯一ID
     *
     * @param id 道具唯一ID
     */
    public void setId(long id) {
        this.id = id;
    }

    // 道具模板ID
    private int templateId = 0;
    
    /**
     * 获取道具模板ID
     *
     * @return 道具模板ID
     */
    public int getTemplateId() {
        return templateId;
    }
    
    /**
     * 设置道具模板ID
     *
     * @param templateId 道具模板ID
     */
    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    // 道具数量
    private int count = 0;
    
    /**
     * 获取道具数量
     *
     * @return 道具数量
     */
    public int getCount() {
        return count;
    }
    
    /**
     * 设置道具数量
     *
     * @param count 道具数量
     */
    public void setCount(int count) {
        this.count = count;
    }

    // 所在区域
    private int range = 0;
    
    /**
     * 获取所在区域
     *
     * @return 所在区域
     */
    public int getRange() {
        return range;
    }
    
    /**
     * 设置所在区域
     *
     * @param range 所在区域
     */
    public void setRange(int range) {
        this.range = range;
    }

    // 所在格子位置
    private int slot = 0;
    
    /**
     * 获取所在格子位置
     *
     * @return 所在格子位置
     */
    public int getSlot() {
        return slot;
    }
    
    /**
     * 设置所在格子位置
     *
     * @param slot 所在格子位置
     */
    public void setSlot(int slot) {
        this.slot = slot;
    }

    @Override
    public void writeTo(CodedOutputStream output) {
        if (id != 0) {
            output.writeFixed64(9, id);
        }
        if (templateId != 0) {
            output.writeInt32(16, templateId);
        }
        if (count != 0) {
            output.writeInt32(24, count);
        }
        if (range != 0) {
            output.writeInt32(32, range);
        }
        if (slot != 0) {
            output.writeInt32(40, slot);
        }

    }

    @Override
    public void readFrom(CodedInputStream input) {
        while(true) {
            int tag = input.readTag();
            switch(tag) {
                case 0: {
                    return;
                }
                default: {
                    if (!input.skipField(tag)) {
                        return;
                    }
                    break;
                }
                case 9: {
                    this.id = input.readFixed64();
                    break;
                }
                case 16: {
                    this.templateId = input.readInt32();
                    break;
                }
                case 24: {
                    this.count = input.readInt32();
                    break;
                }
                case 32: {
                    this.range = input.readInt32();
                    break;
                }
                case 40: {
                    this.slot = input.readInt32();
                    break;
                }
            }
        }
    }

}
