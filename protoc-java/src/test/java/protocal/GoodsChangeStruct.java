/*
 * Copyright © 2016 MoKun All Rights Reserved.
 * 
 * 感谢您加入墨鹍科技，不用多久，您就会升职加薪、当上总经理、出任CEO、迎娶白富美、从此走上人生巅峰
 * 除非符合本公司的商业许可协议，否则不得使用或传播此源码，您可以下载许可协议文件：
 * 
 * 		http://www.noark.xyz/mokun/LICENSE
 *
 * 1、未经许可，任何公司及个人不得以任何方式或理由来修改、使用或传播此源码;
 * 2、禁止在本源码或其他相关源码的基础上发展任何派生版本、修改版本或第三方版本;
 * 3、无论你对源代码做出任何修改和优化，版权都归墨鹍科技所有，我们将保留所有权利;
 * 4、凡侵犯墨鹍科技相关版权或著作权等知识产权者，必依法追究其法律责任，特此郑重法律声明！
 */
package protocal;

import xyz.noark.protobuf.*;

/**
 * 道具变化结构体（用于移动，合并消耗等）
 *
 * @author 自动化工具(墨鹍科技)
 */
public class GoodsChangeStruct implements ProtobufSerializable {
    // 道具唯一ID
    private long id = 0;
    
    /**
     * 获取道具唯一ID
     *
     * @return 道具唯一ID
     */
    public long getId() {
        return id;
    }
    
    /**
     * 设置道具唯一ID
     *
     * @param id 道具唯一ID
     */
    public void setId(long id) {
        this.id = id;
    }

    // 数量为0时就是删除此道具
    private int count = 0;
    
    /**
     * 获取数量为0时就是删除此道具
     *
     * @return 数量为0时就是删除此道具
     */
    public int getCount() {
        return count;
    }
    
    /**
     * 设置数量为0时就是删除此道具
     *
     * @param count 数量为0时就是删除此道具
     */
    public void setCount(int count) {
        this.count = count;
    }

    private int slotNum = 0;
    
    public int getSlotNum() {
        return slotNum;
    }
    
    public void setSlotNum(int slotNum) {
        this.slotNum = slotNum;
    }

    @Override
    public void writeTo(CodedOutputStream output) {
        if (id != 0) {
            output.writeInt64(8, id);
        }
        if (count != 0) {
            output.writeInt32(16, count);
        }
        if (slotNum != 0) {
            output.writeInt32(24, slotNum);
        }

    }

    @Override
    public void readFrom(CodedInputStream input) {
        while(true) {
            int tag = input.readTag();
            switch(tag) {
                case 0: {
                    return;
                }
                default: {
                    if (!input.skipField(tag)) {
                        return;
                    }
                    break;
                }
                case 8: {
                    this.id = input.readInt64();
                    break;
                }
                case 16: {
                    this.count = input.readInt32();
                    break;
                }
                case 24: {
                    this.slotNum = input.readInt32();
                    break;
                }
            }
        }
    }

}
