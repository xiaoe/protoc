/*
 * Copyright © 2016 MoKun All Rights Reserved.
 * 
 * 感谢您加入墨鹍科技，不用多久，您就会升职加薪、当上总经理、出任CEO、迎娶白富美、从此走上人生巅峰
 * 除非符合本公司的商业许可协议，否则不得使用或传播此源码，您可以下载许可协议文件：
 * 
 * 		http://www.noark.xyz/mokun/LICENSE
 *
 * 1、未经许可，任何公司及个人不得以任何方式或理由来修改、使用或传播此源码;
 * 2、禁止在本源码或其他相关源码的基础上发展任何派生版本、修改版本或第三方版本;
 * 3、无论你对源代码做出任何修改和优化，版权都归墨鹍科技所有，我们将保留所有权利;
 * 4、凡侵犯墨鹍科技相关版权或著作权等知识产权者，必依法追究其法律责任，特此郑重法律声明！
 */
package protocal;

import java.util.ArrayList;
import java.util.List;
import xyz.noark.protobuf.*;

/**
 * 创建玩家出生旗子
 *
 * @author 自动化工具(墨鹍科技)
 */
public class FlagStruct implements ProtobufSerializable {
    // 唯一ID
    private int id = 0;
    
    /**
     * 获取唯一ID
     *
     * @return 唯一ID
     */
    public int getId() {
        return id;
    }
    
    /**
     * 设置唯一ID
     *
     * @param id 唯一ID
     */
    public void setId(int id) {
        this.id = id;
    }

    private List<String> name = new ArrayList<>();
    
    public List<String> getName() {
        return name;
    }
    

    // 阵营
    private int camp = 0;
    
    /**
     * 获取阵营
     *
     * @return 阵营
     */
    public int getCamp() {
        return camp;
    }
    
    /**
     * 设置阵营
     *
     * @param camp 阵营
     */
    public void setCamp(int camp) {
        this.camp = camp;
    }

    // 移动到X坐标
    private int x = 0;
    
    /**
     * 获取移动到X坐标
     *
     * @return 移动到X坐标
     */
    public int getX() {
        return x;
    }
    
    /**
     * 设置移动到X坐标
     *
     * @param x 移动到X坐标
     */
    public void setX(int x) {
        this.x = x;
    }

    // 移动到Y坐标
    private int y = 0;
    
    /**
     * 获取移动到Y坐标
     *
     * @return 移动到Y坐标
     */
    public int getY() {
        return y;
    }
    
    /**
     * 设置移动到Y坐标
     *
     * @param y 移动到Y坐标
     */
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void writeTo(CodedOutputStream output) {
        if (id != 0) {
            output.writeInt32(8, id);
        }
        if (name.size() > 0) {
            output.writeStringList(18, name, false);
        }
        if (camp != 0) {
            output.writeInt32(24, camp);
        }
        if (x != 0) {
            output.writeInt32(32, x);
        }
        if (y != 0) {
            output.writeInt32(40, y);
        }

    }

    @Override
    public void readFrom(CodedInputStream input) {
        while(true) {
            int tag = input.readTag();
            switch(tag) {
                case 0: {
                    return;
                }
                default: {
                    if (!input.skipField(tag)) {
                        return;
                    }
                    break;
                }
                case 8: {
                    this.id = input.readInt32();
                    break;
                }
                case 18: {
                    name.add(input.readString());
                    break;
                }
                case 24: {
                    this.camp = input.readInt32();
                    break;
                }
                case 32: {
                    this.x = input.readInt32();
                    break;
                }
                case 40: {
                    this.y = input.readInt32();
                    break;
                }
            }
        }
    }

}
