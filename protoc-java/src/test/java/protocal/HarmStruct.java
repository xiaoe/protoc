/*
 * Copyright © 2016 MoKun All Rights Reserved.
 * 
 * 感谢您加入墨鹍科技，不用多久，您就会升职加薪、当上总经理、出任CEO、迎娶白富美、从此走上人生巅峰
 * 除非符合本公司的商业许可协议，否则不得使用或传播此源码，您可以下载许可协议文件：
 * 
 * 		http://www.noark.xyz/mokun/LICENSE
 *
 * 1、未经许可，任何公司及个人不得以任何方式或理由来修改、使用或传播此源码;
 * 2、禁止在本源码或其他相关源码的基础上发展任何派生版本、修改版本或第三方版本;
 * 3、无论你对源代码做出任何修改和优化，版权都归墨鹍科技所有，我们将保留所有权利;
 * 4、凡侵犯墨鹍科技相关版权或著作权等知识产权者，必依法追究其法律责任，特此郑重法律声明！
 */
package protocal;

import xyz.noark.protobuf.*;

/**
 * 伤害
 *
 * @author 自动化工具(墨鹍科技)
 */
public class HarmStruct implements ProtobufSerializable {
    // 唯一ID
    private int id = 0;
    
    /**
     * 获取唯一ID
     *
     * @return 唯一ID
     */
    public int getId() {
        return id;
    }
    
    /**
     * 设置唯一ID
     *
     * @param id 唯一ID
     */
    public void setId(int id) {
        this.id = id;
    }

    // 掉多少血
    private int decrHp = 0;
    
    /**
     * 获取掉多少血
     *
     * @return 掉多少血
     */
    public int getDecrHp() {
        return decrHp;
    }
    
    /**
     * 设置掉多少血
     *
     * @param decrHp 掉多少血
     */
    public void setDecrHp(int decrHp) {
        this.decrHp = decrHp;
    }

    // 还有多少血
    private int curHp = 0;
    
    /**
     * 获取还有多少血
     *
     * @return 还有多少血
     */
    public int getCurHp() {
        return curHp;
    }
    
    /**
     * 设置还有多少血
     *
     * @param curHp 还有多少血
     */
    public void setCurHp(int curHp) {
        this.curHp = curHp;
    }

    @Override
    public void writeTo(CodedOutputStream output) {
        if (id != 0) {
            output.writeInt32(8, id);
        }
        if (decrHp != 0) {
            output.writeInt32(16, decrHp);
        }
        if (curHp != 0) {
            output.writeInt32(24, curHp);
        }

    }

    @Override
    public void readFrom(CodedInputStream input) {
        while(true) {
            int tag = input.readTag();
            switch(tag) {
                case 0: {
                    return;
                }
                default: {
                    if (!input.skipField(tag)) {
                        return;
                    }
                    break;
                }
                case 8: {
                    this.id = input.readInt32();
                    break;
                }
                case 16: {
                    this.decrHp = input.readInt32();
                    break;
                }
                case 24: {
                    this.curHp = input.readInt32();
                    break;
                }
            }
        }
    }

}
