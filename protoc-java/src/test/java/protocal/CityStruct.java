/*
 * Copyright © 2016 MoKun All Rights Reserved.
 * 
 * 感谢您加入墨鹍科技，不用多久，您就会升职加薪、当上总经理、出任CEO、迎娶白富美、从此走上人生巅峰
 * 除非符合本公司的商业许可协议，否则不得使用或传播此源码，您可以下载许可协议文件：
 * 
 * 		http://www.noark.xyz/mokun/LICENSE
 *
 * 1、未经许可，任何公司及个人不得以任何方式或理由来修改、使用或传播此源码;
 * 2、禁止在本源码或其他相关源码的基础上发展任何派生版本、修改版本或第三方版本;
 * 3、无论你对源代码做出任何修改和优化，版权都归墨鹍科技所有，我们将保留所有权利;
 * 4、凡侵犯墨鹍科技相关版权或著作权等知识产权者，必依法追究其法律责任，特此郑重法律声明！
 */
package protocal;

import xyz.noark.protobuf.*;

/**
 * 世界地图上的城市
 *
 * @author 自动化工具(墨鹍科技)
 */
public class CityStruct implements ProtobufSerializable {
    // 城市ID
    private int id = 0;
    
    /**
     * 获取城市ID
     *
     * @return 城市ID
     */
    public int getId() {
        return id;
    }
    
    /**
     * 设置城市ID
     *
     * @param id 城市ID
     */
    public void setId(int id) {
        this.id = id;
    }

    // 国家
    private int country = 0;
    
    /**
     * 获取国家
     *
     * @return 国家
     */
    public int getCountry() {
        return country;
    }
    
    /**
     * 设置国家
     *
     * @param country 国家
     */
    public void setCountry(int country) {
        this.country = country;
    }

    // 攻击方人数
    private int attCount = 0;
    
    /**
     * 获取攻击方人数
     *
     * @return 攻击方人数
     */
    public int getAttCount() {
        return attCount;
    }
    
    /**
     * 设置攻击方人数
     *
     * @param attCount 攻击方人数
     */
    public void setAttCount(int attCount) {
        this.attCount = attCount;
    }

    // 防御方人数
    private int defCount = 0;
    
    /**
     * 获取防御方人数
     *
     * @return 防御方人数
     */
    public int getDefCount() {
        return defCount;
    }
    
    /**
     * 设置防御方人数
     *
     * @param defCount 防御方人数
     */
    public void setDefCount(int defCount) {
        this.defCount = defCount;
    }

    // 城主名称
    private String seignior = "";
    
    /**
     * 获取城主名称
     *
     * @return 城主名称
     */
    public String getSeignior() {
        return seignior;
    }
    
    /**
     * 设置城主名称
     *
     * @param seignior 城主名称
     */
    public void setSeignior(String seignior) {
        this.seignior = seignior;
    }

    // 防御NPC数量
    private int defNpcCount = 0;
    
    /**
     * 获取防御NPC数量
     *
     * @return 防御NPC数量
     */
    public int getDefNpcCount() {
        return defNpcCount;
    }
    
    /**
     * 设置防御NPC数量
     *
     * @param defNpcCount 防御NPC数量
     */
    public void setDefNpcCount(int defNpcCount) {
        this.defNpcCount = defNpcCount;
    }

    // 0-100的比例值
    private int cityHp = 0;
    
    /**
     * 获取0-100的比例值
     *
     * @return 0-100的比例值
     */
    public int getCityHp() {
        return cityHp;
    }
    
    /**
     * 设置0-100的比例值
     *
     * @param cityHp 0-100的比例值
     */
    public void setCityHp(int cityHp) {
        this.cityHp = cityHp;
    }

    // 城市是否在打仗
    private int isFighting = 0;
    
    /**
     * 获取城市是否在打仗
     *
     * @return 城市是否在打仗
     */
    public int getIsFighting() {
        return isFighting;
    }
    
    /**
     * 设置城市是否在打仗
     *
     * @param isFighting 城市是否在打仗
     */
    public void setIsFighting(int isFighting) {
        this.isFighting = isFighting;
    }

    @Override
    public void writeTo(CodedOutputStream output) {
        if (id != 0) {
            output.writeInt32(8, id);
        }
        if (country != 0) {
            output.writeInt32(16, country);
        }
        if (attCount != 0) {
            output.writeInt32(24, attCount);
        }
        if (defCount != 0) {
            output.writeInt32(32, defCount);
        }
        if (seignior.length() > 0) {
            output.writeString(42, seignior);
        }
        if (defNpcCount != 0) {
            output.writeInt32(48, defNpcCount);
        }
        if (cityHp != 0) {
            output.writeInt32(56, cityHp);
        }
        if (isFighting != 0) {
            output.writeInt32(64, isFighting);
        }

    }

    @Override
    public void readFrom(CodedInputStream input) {
        while(true) {
            int tag = input.readTag();
            switch(tag) {
                case 0: {
                    return;
                }
                default: {
                    if (!input.skipField(tag)) {
                        return;
                    }
                    break;
                }
                case 8: {
                    this.id = input.readInt32();
                    break;
                }
                case 16: {
                    this.country = input.readInt32();
                    break;
                }
                case 24: {
                    this.attCount = input.readInt32();
                    break;
                }
                case 32: {
                    this.defCount = input.readInt32();
                    break;
                }
                case 42: {
                    this.seignior = input.readString();
                    break;
                }
                case 48: {
                    this.defNpcCount = input.readInt32();
                    break;
                }
                case 56: {
                    this.cityHp = input.readInt32();
                    break;
                }
                case 64: {
                    this.isFighting = input.readInt32();
                    break;
                }
            }
        }
    }

}
