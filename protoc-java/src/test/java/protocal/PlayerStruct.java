/*
 * Copyright © 2016 MoKun All Rights Reserved.
 * 
 * 感谢您加入墨鹍科技，不用多久，您就会升职加薪、当上总经理、出任CEO、迎娶白富美、从此走上人生巅峰
 * 除非符合本公司的商业许可协议，否则不得使用或传播此源码，您可以下载许可协议文件：
 * 
 * 		http://www.noark.xyz/mokun/LICENSE
 *
 * 1、未经许可，任何公司及个人不得以任何方式或理由来修改、使用或传播此源码;
 * 2、禁止在本源码或其他相关源码的基础上发展任何派生版本、修改版本或第三方版本;
 * 3、无论你对源代码做出任何修改和优化，版权都归墨鹍科技所有，我们将保留所有权利;
 * 4、凡侵犯墨鹍科技相关版权或著作权等知识产权者，必依法追究其法律责任，特此郑重法律声明！
 */
package protocal;

import xyz.noark.protobuf.*;

/**
 * 登录时的玩家信息
 *
 * @author 自动化工具(墨鹍科技)
 */
public class PlayerStruct implements ProtobufSerializable {
    // 角色ID
    private long id = 0;
    
    /**
     * 获取角色ID
     *
     * @return 角色ID
     */
    public long getId() {
        return id;
    }
    
    /**
     * 设置角色ID
     *
     * @param id 角色ID
     */
    public void setId(long id) {
        this.id = id;
    }

    // 角色名称
    private String name = "";
    
    /**
     * 获取角色名称
     *
     * @return 角色名称
     */
    public String getName() {
        return name;
    }
    
    /**
     * 设置角色名称
     *
     * @param name 角色名称
     */
    public void setName(String name) {
        this.name = name;
    }

    // 性别
    private int gender = 0;
    
    /**
     * 获取性别
     *
     * @return 性别
     */
    public int getGender() {
        return gender;
    }
    
    /**
     * 设置性别
     *
     * @param gender 性别
     */
    public void setGender(int gender) {
        this.gender = gender;
    }

    // 阵营
    private int camp = 0;
    
    /**
     * 获取阵营
     *
     * @return 阵营
     */
    public int getCamp() {
        return camp;
    }
    
    /**
     * 设置阵营
     *
     * @param camp 阵营
     */
    public void setCamp(int camp) {
        this.camp = camp;
    }

    // 等级
    private int level = 0;
    
    /**
     * 获取等级
     *
     * @return 等级
     */
    public int getLevel() {
        return level;
    }
    
    /**
     * 设置等级
     *
     * @param level 等级
     */
    public void setLevel(int level) {
        this.level = level;
    }

    // VIP等级
    private int vipLevel = 0;
    
    /**
     * 获取VIP等级
     *
     * @return VIP等级
     */
    public int getVipLevel() {
        return vipLevel;
    }
    
    /**
     * 设置VIP等级
     *
     * @param vipLevel VIP等级
     */
    public void setVipLevel(int vipLevel) {
        this.vipLevel = vipLevel;
    }

    // 公会名称
    private String guildName = "";
    
    /**
     * 获取公会名称
     *
     * @return 公会名称
     */
    public String getGuildName() {
        return guildName;
    }
    
    /**
     * 设置公会名称
     *
     * @param guildName 公会名称
     */
    public void setGuildName(String guildName) {
        this.guildName = guildName;
    }

    @Override
    public void writeTo(CodedOutputStream output) {
        if (id != 0) {
            output.writeFixed64(9, id);
        }
        if (name.length() > 0) {
            output.writeString(18, name);
        }
        if (gender != 0) {
            output.writeInt32(32, gender);
        }
        if (camp != 0) {
            output.writeInt32(40, camp);
        }
        if (level != 0) {
            output.writeInt32(48, level);
        }
        if (vipLevel != 0) {
            output.writeInt32(56, vipLevel);
        }
        if (guildName.length() > 0) {
            output.writeString(66, guildName);
        }

    }

    @Override
    public void readFrom(CodedInputStream input) {
        while(true) {
            int tag = input.readTag();
            switch(tag) {
                case 0: {
                    return;
                }
                default: {
                    if (!input.skipField(tag)) {
                        return;
                    }
                    break;
                }
                case 9: {
                    this.id = input.readFixed64();
                    break;
                }
                case 18: {
                    this.name = input.readString();
                    break;
                }
                case 32: {
                    this.gender = input.readInt32();
                    break;
                }
                case 40: {
                    this.camp = input.readInt32();
                    break;
                }
                case 48: {
                    this.level = input.readInt32();
                    break;
                }
                case 56: {
                    this.vipLevel = input.readInt32();
                    break;
                }
                case 66: {
                    this.guildName = input.readString();
                    break;
                }
            }
        }
    }

}
