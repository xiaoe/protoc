/*
 * Copyright © 2016 MoKun All Rights Reserved.
 * 
 * 感谢您加入墨鹍科技，不用多久，您就会升职加薪、当上总经理、出任CEO、迎娶白富美、从此走上人生巅峰
 * 除非符合本公司的商业许可协议，否则不得使用或传播此源码，您可以下载许可协议文件：
 * 
 * 		http://www.noark.xyz/mokun/LICENSE
 *
 * 1、未经许可，任何公司及个人不得以任何方式或理由来修改、使用或传播此源码;
 * 2、禁止在本源码或其他相关源码的基础上发展任何派生版本、修改版本或第三方版本;
 * 3、无论你对源代码做出任何修改和优化，版权都归墨鹍科技所有，我们将保留所有权利;
 * 4、凡侵犯墨鹍科技相关版权或著作权等知识产权者，必依法追究其法律责任，特此郑重法律声明！
 */
package protocal;

import java.util.ArrayList;
import java.util.List;
import protocal.BuffStruct;
import xyz.noark.protobuf.*;

/**
 * 创建部队结构体
 *
 * @author 自动化工具(墨鹍科技)
 */
public class TroopsStruct implements ProtobufSerializable {
    // 唯一ID
    private int id = 0;
    
    /**
     * 获取唯一ID
     *
     * @return 唯一ID
     */
    public int getId() {
        return id;
    }
    
    /**
     * 设置唯一ID
     *
     * @param id 唯一ID
     */
    public void setId(int id) {
        this.id = id;
    }

    // 模板ID
    private int templateId = 0;
    
    /**
     * 获取模板ID
     *
     * @return 模板ID
     */
    public int getTemplateId() {
        return templateId;
    }
    
    /**
     * 设置模板ID
     *
     * @param templateId 模板ID
     */
    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    // 方向
    private int direction = 0;
    
    /**
     * 获取方向
     *
     * @return 方向
     */
    public int getDirection() {
        return direction;
    }
    
    /**
     * 设置方向
     *
     * @param direction 方向
     */
    public void setDirection(int direction) {
        this.direction = direction;
    }

    // 阵营
    private int camp = 0;
    
    /**
     * 获取阵营
     *
     * @return 阵营
     */
    public int getCamp() {
        return camp;
    }
    
    /**
     * 设置阵营
     *
     * @param camp 阵营
     */
    public void setCamp(int camp) {
        this.camp = camp;
    }

    // 时间（速度的转化值）
    private int time = 0;
    
    /**
     * 获取时间（速度的转化值）
     *
     * @return 时间（速度的转化值）
     */
    public int getTime() {
        return time;
    }
    
    /**
     * 设置时间（速度的转化值）
     *
     * @param time 时间（速度的转化值）
     */
    public void setTime(int time) {
        this.time = time;
    }

    // 移动到X坐标
    private int x = 0;
    
    /**
     * 获取移动到X坐标
     *
     * @return 移动到X坐标
     */
    public int getX() {
        return x;
    }
    
    /**
     * 设置移动到X坐标
     *
     * @param x 移动到X坐标
     */
    public void setX(int x) {
        this.x = x;
    }

    // 移动到Y坐标
    private int y = 0;
    
    /**
     * 获取移动到Y坐标
     *
     * @return 移动到Y坐标
     */
    public int getY() {
        return y;
    }
    
    /**
     * 设置移动到Y坐标
     *
     * @param y 移动到Y坐标
     */
    public void setY(int y) {
        this.y = y;
    }

    // HP
    private int hp = 0;
    
    /**
     * 获取HP
     *
     * @return HP
     */
    public int getHp() {
        return hp;
    }
    
    /**
     * 设置HP
     *
     * @param hp HP
     */
    public void setHp(int hp) {
        this.hp = hp;
    }

    // 最大HP
    private int maxHp = 0;
    
    /**
     * 获取最大HP
     *
     * @return 最大HP
     */
    public int getMaxHp() {
        return maxHp;
    }
    
    /**
     * 设置最大HP
     *
     * @param maxHp 最大HP
     */
    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
    }

    // Buff
    private List<BuffStruct> buffs = new ArrayList<>();
    
    /**
     * 获取Buff
     *
     * @return Buff
     */
    public List<BuffStruct> getBuffs() {
        return buffs;
    }
    

    @Override
    public void writeTo(CodedOutputStream output) {
        if (id != 0) {
            output.writeInt32(8, id);
        }
        if (templateId != 0) {
            output.writeInt32(16, templateId);
        }
        if (direction != 0) {
            output.writeInt32(24, direction);
        }
        if (camp != 0) {
            output.writeInt32(32, camp);
        }
        if (time != 0) {
            output.writeInt32(40, time);
        }
        if (x != 0) {
            output.writeInt32(48, x);
        }
        if (y != 0) {
            output.writeInt32(56, y);
        }
        if (hp != 0) {
            output.writeInt32(64, hp);
        }
        if (maxHp != 0) {
            output.writeInt32(72, maxHp);
        }
        if (buffs.size() > 0) {
            output.writeMessageList(82, buffs, false);
        }

    }

    @Override
    public void readFrom(CodedInputStream input) {
        while(true) {
            int tag = input.readTag();
            switch(tag) {
                case 0: {
                    return;
                }
                default: {
                    if (!input.skipField(tag)) {
                        return;
                    }
                    break;
                }
                case 8: {
                    this.id = input.readInt32();
                    break;
                }
                case 16: {
                    this.templateId = input.readInt32();
                    break;
                }
                case 24: {
                    this.direction = input.readInt32();
                    break;
                }
                case 32: {
                    this.camp = input.readInt32();
                    break;
                }
                case 40: {
                    this.time = input.readInt32();
                    break;
                }
                case 48: {
                    this.x = input.readInt32();
                    break;
                }
                case 56: {
                    this.y = input.readInt32();
                    break;
                }
                case 64: {
                    this.hp = input.readInt32();
                    break;
                }
                case 72: {
                    this.maxHp = input.readInt32();
                    break;
                }
                case 82: {
                    buffs.add(input.readMessage(new BuffStruct()));
                    break;
                }
            }
        }
    }

}
