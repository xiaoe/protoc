package com.google.protobuf.compiler.as3;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.protobuf.DescriptorProtos.DescriptorProto;
import com.google.protobuf.DescriptorProtos.EnumDescriptorProto;
import com.google.protobuf.DescriptorProtos.FileDescriptorProto;
import com.google.protobuf.DescriptorProtos.SourceCodeInfo;
import com.google.protobuf.compiler.PluginProtos.CodeGeneratorRequest;
import com.google.protobuf.compiler.PluginProtos.CodeGeneratorResponse;

public class CodeGenerator {
	private static final List<String> allClassName = new ArrayList<>(512);

	public static List<String> getAllclassname() {
		return allClassName;
	}

	public static void main(String[] args) {
		new CodeGenerator();

		// 额外生成一个AS类.
		StringBuilder sb = new StringBuilder(2048);
		sb.append("package protocal\n");
		sb.append("{\n\n");

		sb.append("/**\n");
		sb.append(" * 加入引用的类\n");
		sb.append(" * @author hua.wu\n");
		sb.append(" */\n");
		sb.append("public class ProtocalManager\n");
		sb.append("{\n");
		sb.append("	/** 将服务端发来的_SC文件写入 **/\n");
		sb.append("	public function ProtocalManager()\n");
		sb.append("	{\n");
		for (String c : allClassName) {
			sb.append("\t\t").append(c).append(";\n");
		}
		sb.append("	}\n");
		sb.append("}\n");
		sb.append("}");

		try {
			File file = new File("../ProtocalManager.as");
			if (!file.exists())
				file.createNewFile();
			FileOutputStream out = new FileOutputStream(file, false); // 如果追加方式用true
			out.write(sb.toString().getBytes("utf-8"));// 注意需要转换对应的字符集
			out.flush();
			out.close();
		} catch (IOException ex) {
			System.out.println(ex.getStackTrace());
		}
	}

	private HashMap<String, String> _classRef = new HashMap<>();
	private HashMap<String, IGenerator> _fileToGenerate = new HashMap<>();

	public CodeGenerator() {
		try {
			CodeGeneratorRequest request = CodeGeneratorRequest.parseFrom(System.in);
			CodeGeneratorResponse.Builder response = CodeGeneratorResponse.newBuilder();

			for (FileDescriptorProto proto : request.getProtoFileList()) {
				buildClassRefs(proto);
			}

			for (FileDescriptorProto proto : request.getProtoFileList()) {
				if (request.getFileToGenerateList().contains(proto.getName())) {
					if ("proto3".equals(proto.getSyntax())) {
						buildMessageGenerators(proto);
					} else {
						throw new Exception("need syntax=\"proto3\": " + proto.getName());
					}
				}
			}

			for (Map.Entry<String, IGenerator> entry : _fileToGenerate.entrySet()) {
				Printer printer = new Printer(0);
				entry.getValue().generate(printer);

				CodeGeneratorResponse.File.Builder file = CodeGeneratorResponse.File.newBuilder();
				file.setName(entry.getKey().replace('.', '/') + ".as");
				file.setContent(printer.toString());
				response.addFile(file);
			}

			response.build().writeTo(System.out);
			System.out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void buildClassRefs(FileDescriptorProto fileProto) {
		String packageName = fileProto.getPackage();

		for (DescriptorProto proto : fileProto.getMessageTypeList()) {
			addClassRef(packageName, "", proto);
		}

		for (EnumDescriptorProto proto : fileProto.getEnumTypeList()) {
			addEnumRef(packageName, "", proto);
		}
	}

	private void addClassRef(String packageName, String scope, DescriptorProto descriptor) {
		String name = descriptor.getName();
		if (Util.isMapEntry(descriptor)) {
			_classRef.put(Util.makeQualifiedClassName(packageName, scope, name), Util.makeASQualifiedClassName(packageName, scope, name));
		} else {
			_classRef.put(Util.makeQualifiedClassName(packageName, scope, name), Util.makeASQualifiedClassName(packageName, scope, name));

			for (DescriptorProto nested : descriptor.getNestedTypeList()) {
				addClassRef(packageName, Util.makeScope(scope, name), nested);
			}

			for (EnumDescriptorProto nested : descriptor.getEnumTypeList()) {
				addEnumRef(packageName, Util.makeScope(scope, name), nested);
			}
		}
	}

	private void addEnumRef(String packageName, String scope, EnumDescriptorProto descriptor) {
		String name = descriptor.getName();
		_classRef.put(Util.makeQualifiedClassName(packageName, scope, name), Util.makeASQualifiedClassName(packageName, scope, name));
	}

	private void buildMessageGenerators(FileDescriptorProto fileProto) {
		String packageName = fileProto.getPackage();

		for (int i = 0; i < fileProto.getMessageTypeCount(); i++) {
			DescriptorProto descriptor = fileProto.getMessageTypeList().get(i);
			addMessageGenerator(packageName, "", descriptor, fileProto.getSourceCodeInfo(), i);
		}

		for (EnumDescriptorProto descriptor : fileProto.getEnumTypeList()) {
			_fileToGenerate.put(Util.makeASQualifiedClassName(packageName, "", descriptor.getName()), new EnumGenerator(descriptor, packageName, ""));
		}
	}

	private void addMessageGenerator(String packageName, String scope, DescriptorProto descriptor, SourceCodeInfo sourceCodeInfo, int i) {
		String name = descriptor.getName();
		if (descriptor.hasOptions() && descriptor.getOptions().getMapEntry()) {
			_fileToGenerate.put(Util.makeASQualifiedClassName(packageName, scope, name), new MapGenerator(descriptor, _classRef, packageName, scope));
		} else {
			_fileToGenerate.put(Util.makeASQualifiedClassName(packageName, scope, name), new MessageGenerator(descriptor, _classRef, packageName, scope, sourceCodeInfo, i));

			scope = Util.makeScope(scope, name);

			for (DescriptorProto nestedProto : descriptor.getNestedTypeList()) {
				addMessageGenerator(packageName, scope, nestedProto, sourceCodeInfo, i);
			}

			for (EnumDescriptorProto nestedEnum : descriptor.getEnumTypeList()) {
				_fileToGenerate.put(Util.makeASQualifiedClassName(packageName, scope, nestedEnum.getName()), new EnumGenerator(nestedEnum, packageName, scope));
			}
		}
	}
}
